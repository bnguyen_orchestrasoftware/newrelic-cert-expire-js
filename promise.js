const request = require('request');

var websites_array = [
"https://expired.badssl.com",
"https://lazymagnolia.orchestratedbeer.com"
]

websites_array.forEach((x) => {

  const promiseAPI = () => new Promise((resolve, reject) => {
    request(
      {
        method: 'GET',
        uri: x,
        strictSSL: true
      },
      function (error, response, body) {
        if(error){
          error.website = x
          reject({errorMessage: error, website: error.website})
        }
        else{
          response.website = x
          resolve({response: response, website: response.website})
        }
      }); //end request
  });//end promiseAPI


  promiseAPI()
    .then((response) => {
      console.log("Valid certificate for " + response.website)
    })
    .catch((error) => {
      console.log("!!! Invalid certificate for : " + error.website)
      // throw ("expired certificate " + error.website);
    })
})
