var https = require('https');

var options = {
  host: 'lazymagnolia.orchestratedbeer.com',
  method: 'get',
  path: '/'
};

var req = https.request(options,
  function (res) {
    console.log('certificate authorized:' + res.socket.authorized);
  });

req.end();
